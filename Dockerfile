FROM ubuntu:22.04

ARG PHP_VERSION=7.3
ARG TZ=America/Sao_Paulo

ENV TZ ${TZ}
ENV PHP_VERSION ${PHP_VERSION}
ENV DEBIAN_FRONTEND noninteractive

RUN echo ${TZ} >/etc/timezone
RUN apt update && apt -y upgrade
# PHP 7.[34] Repository
RUN if [ "${PHP_VERSION}" = "7.3" ] || [ "${PHP_VERSION}" = "7.4" ]; then \
		apt -y install software-properties-common && \
		add-apt-repository ppa:ondrej/php && \
		apt update; \
	fi
# Tools
RUN apt -y --no-install-recommends install \
        curl \
        ca-certificates \
        zip \
        unzip \
        git \
		cron \
		vim \
        supervisor
# Apache and PHP
RUN apt -y --no-install-recommends install \
        apache2 \
        php${PHP_VERSION}-fpm php${PHP_VERSION}-memcache php${PHP_VERSION}-mysql \
		php${PHP_VERSION}-xml php${PHP_VERSION}-gd php${PHP_VERSION}-mbstring \
		php${PHP_VERSION}-bcmath php${PHP_VERSION}-zip php${PHP_VERSION}-curl php${PHP_VERSION}-xdebug
# Apache Modules
RUN a2enconf php${PHP_VERSION}-fpm && \
	a2enmod proxy && \
    a2enmod proxy_fcgi && \
    a2enmod rewrite
# MariaDB
RUN apt -y --no-install-recommends install \
    mariadb-client-10.6


# Supervisor
RUN mkdir -p /run/php/
COPY config/supervisord/supervisord.conf /etc/supervisor/supervisord.conf
COPY config/supervisord/conf.d/apache.conf /etc/supervisor/conf.d/
COPY config/supervisord/conf.d/php${PHP_VERSION}-fpm.conf /etc/supervisor/conf.d/

# Apache Configuration
COPY config/apache/000-default.conf /etc/apache2/sites-available/000-default.conf
RUN echo PHP_VERSION=${PHP_VERSION} >> /etc/apache2/envvars
RUN echo ServerName 127.0.0.1 >> /etc/apache2/apache2.conf

# PHP Configuration
COPY config/php/fpm/php.ini /etc/php/${PHP_VERSION}/fpm/php.ini
COPY config/php/fpm/pool.d/php${PHP_VERSION}-www.conf /etc/php/${PHP_VERSION}/fpm/pool.d/www.conf
COPY config/php/fpm/xdebug.ini /etc/php/${PHP_VERSION}/mods-available/xdebug.ini

# Cron Configuration
COPY config/cron/crontab /tmp/crontab
RUN cat /tmp/crontab | crontab -

# Startup script to change uid/gid (if environment variable passed) and start supervisord in foreground
COPY start.sh /start.sh
RUN chmod 755 /start.sh

EXPOSE 80
EXPOSE 443

ENTRYPOINT [ "/bin/bash", "/start.sh" ]

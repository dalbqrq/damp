# DAMP - Docker, Apache, MariaDB and PHP

Ambiente LAMP para Docker.

(Criado a partir do projeto: https://github.com/OpenDataStack/docker-apache-php-fpm)

## Resumo

Ambiente em Docker para Apache 2.4 e php-fpm 7.x ou 8.x configurável na criação da imagem do container.

- Baseado no ubuntu:22.04 para facilidade no gerenciamento (não tamanho)
- Apache 2.4 + php-[78].x-fpm
- Customizações de configuração apache 2.4, php e supervisord 
- Base de dados MariaDB


## Uso

### Variáveis

O ambiente pode ser configurado através de variáveis definidas no arquivo .env:

- ENV          : Usado para selecionar arquivos ou diretórios contendo diferentes configurações 
- PHP_VERSION  : Selecione uma das versões do PHP: 7.3, 7.4 or 8.1
- TZ           : Timezone (ex: America/Sao_Paulo)
- DB_ROOT_PASS : Senha do usuário root do banco de dados MariaDB
- DB_USER      : Pode ser usado na configuração do sistema PHP
- DB_PASS      : Pode ser usado na configuração do sistema PHP
- DB_HOST      : Pode ser usado na configuração do sistema PHP
- DB_NAME      : Pode ser usado na configuração do sistema PHP
- DB_PORT      : Pode ser usado na configuração do sistema PHP



## Instalação e Execução:

### Download: 

```
git clone git@gitlab.com:dalbqrq/damp.git && cd damp
```

### Configuração:

Copie o template de arquivo .env e altere os valores nele definidos. Ambientes diferentes (dev x prod) podem ter configurações (incluindo senhas de DB) diferentes.

Edite também o arquivo 'config/cron/crontab' e inclua seus prórpios comandos.

```
cp .env.template .env
# Edite o arquivo .env ...
# Edite o arquivo ./config/cron/crontab ...
```



### Instalação:

```
docker volume create mariadb-data
docker volume create log_apache-data
docker compose build apache-app
```

### Execução:

```
docker compose up -d
```

### Teste:

Acesse o endereço em seu browser: http://localhost/index.php


### Login:

```
docker exec -it apache-php /bin/bash
```

## Desenvolvimento

### Código Fonte e Gereciamento de Banco de Dados:

Para executar um código fonte neste ambiente, posicione os arquivos do projeto PHP de baixi do diretório 'src' deste projeto. Toda alteração será imediatamente refletida no funcionamento do sistema uma vez que esta pasta é mapeada dentro do container apache-php.

Para utilizar o 'phpmyadmin', execute o comando:

```
docker compose --profile dev up -d
```

E acesse o endereço: http://localhost:8099

